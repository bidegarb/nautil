################################################################################
## NAUtil Toolbox                                                             ##
## Author: Brigitte BIDEGARAY-FESQUET                                         ##
## Date: September 2006                                                       ##
##                                                                            ##
## SampleFile DebyeBEP: Maxwell-Debye equations with BEP formulation          ##
################################################################################

# Load Package NAUtil

read NAUtil;

# Algorithm definition

Dim := 1:
Polar := "TM":
Formula := "B_EP":
Model := "MaxDebye":

# Define equations

Faraday(Eq, Dim, Polar):
Ampere(Eq, Dim, Polar, Formula):
NewEq := tr*(P[n+1]-P[n])/dt 
       + 1/2*(P[n+1]+P[n])
       - eps0*(epss-epsinfini)*(E[n+1]+E[n])/2:
CreateEq(Eq, NewEq, Dim, Polar): 
PrintTab(Eq): 
CalcVar(Dim, Polar, Formula, Model, Var, VarBack):

# Characteristic polynomial definition

G := Eq2Ampli(Eq, Var);
phi[0] := Ampli2Poly(G, Z);

# 1D study

if Dim = 1 then
  phi1D := eval(phi):
  save phi1D, "StorePoly/Debye_1D_BEP.m":

  # Polynomial computation, --> Factors and Rests

  vonNeumann(phi, Z):
  facts := PolyFactors(phi, Z):
  rests := PolyRests(phi, facts, Z):
  PrintTab(phi):
  PrintTab(facts):
  PrintTab(rests):

  # There are cancellations when q=0 or etas=1.
  # Sign study in the general case

  printf("-------------------------------------------------------------\n"):
  printf("General case: 0 < delta, 1 < etas, 0 < q (facts), q < 4 (Yee)\n"):
  printf("-------------------------------------------------------------\n"):
  SignCheck(phi, Z, [0 < delta, 1 < etas, 0 < q, q < 4]):

  # Special case: etas = 1

  printf("----------------------\n"):
  printf("Special case: etas = 1\n"):
  printf("----------------------\n"):
  phietas1[0] := eval(phi[0], etas = 1):
  vonNeumann(phietas1, Z):
  psietas1[0] := diff(phietas1[1], Z):
  PrintTab(psietas1):
  SignCheck(psietas1, Z, [0 < delta, 0 < q, q < 4]):
  printf("--> simple von Neumann polynomial\n"): 

  # Special case: q = 0

  printf("-------------------\n"):
  printf("Special case: q = 0\n"): 
  printf("-------------------\n"):
  phiq0[0] := eval(phi[0], q = 0):
  vonNeumann(phiq0, Z):
  psiq0[0] := diff(phiq0[1], Z):
  PrintTab(psiq0):
  printf("We have derived and we only have a simple von Neumann polynomial\n"): 
  printf("--> Direct study on amplification matrix\n"): 
  Gback := evalm(eval(G, VarBack)):
  Gq0 := evalm(eval(Gback, sigma = 0)):
  print(Gq0):
  EigenSpaceDim(Gq0):

  # Special case: q = 4

  printf("-------------------\n"):
  printf("Special case: q = 4\n"):
  printf("-------------------\n"):
  phiq4[0] := eval(phi[0], q = 4):
  vonNeumann(phiq4, Z):
  PrintTab(phiq4):
  SignCheck(phiq4, Z, [0 < delta, 1 < etas]):
  printf("--> simple von Neumann polynomial\n"): 

  # Special case: q = 4, etas = 1

  printf("-----------------------------\n"):
  printf("Special case: q = 4, etas = 1\n"):
  printf("-----------------------------\n"):
  phiq4etas1[0] := eval(phi[0], [q = 4, etas = 1]);
  psiq4etas1[0] := diff(phiq4etas1[0], Z):
  vonNeumann(psiq4etas1, Z):
  PrintTab(psiq4etas1):
  printf("We have derived and we only have a simple von Neumann polynomial\n"): 
  printf("--> Direct study on amplification matrix\n"): 
  Gq4etas1 := evalm(eval(Gback, [etas=1])):
  Gq4etas1 := evalm(eval(Gq4etas1, sigma = 2)):
  print(Gq4etas1):
  EigenSpaceDim(Gq4etas1):
end if:

# 2D study

if Dim = 2 then
  phi2D := eval(phi):
  read "StorePoly/Debye_1D_BEP.m":
  phi[0] := factor(simplify(phi2D[0]/phi1D[0])):
  printf("--> Special study for root 1\n"): 
  printf("--> Special cases from 1D study: etas = 1, q = 0, q = 4\n"): 
  phiZ1 := eval(phi1D[0],[Z=1]):
  printf("--> Only special case to investigate : q = 0\n"): 
  Gback := evalm(eval(G, VarBack)):

  printf("-------------------\n"):
  printf("Special case: q = 0\n"): 
  printf("-------------------\n"):
  Gq0 := evalm(eval(Gback, [sigmax = 0, sigmay = 0])):
  OneEigenSpaceDim(Gq0,1);
end if;

# 3D study

if Dim = 3 then
  phi3D := eval(phi):
  read "StorePoly/Debye_1D_BED.m":
  phi[0] := factor(simplify(phi3D[0]/phi1D[0]/phi1D[0])):  
  printf("--> General case for root 1\n"): 
  OneEigenSpaceDim(G,1);
 
  printf("--> Special cases from 1D study: etas = 1, q = 0, q = 4\n"): 
  phietas1 := factor(eval(phi1D[0],[etas=1])):
  phiq0 := factor(eval(phi1D[0],[q=0])):
  phiq4 := factor(eval(phi1D[0],[q=4])):
  Gback := evalm(eval(G, VarBack)):

  printf("--------------------------\n"):
  printf("Special case: q = 0, Z = 1\n"): 
  printf("--------------------------\n"):
  Gq0 := evalm(eval(Gback, [sigmax = 0, sigmay = 0, sigmaz = 0])):
  OneEigenSpaceDim(Gq0,1);

  printf("---------------------------\n"):
  printf("Special case: q = 4, Z = -1\n"): 
  printf("---------------------------\n"):
  Gq4 := evalm(eval(Gback, [sigmax = 2/sqrt(3), sigmay = 2/sqrt(3), sigmaz = 2/sqrt(3)])):
  OneEigenSpaceDim(Gq4,-1);

  printf("------------------------------------\n"):
  printf("Special case: etas = 1, q = 2, Z = I\n"): 
  printf("------------------------------------\n"):
  Getas1 := evalm(eval(Gback, [etas=1, sigmax = sqrt(2)/sqrt(3), sigmay = sqrt(2)/sqrt(3), sigmaz = sqrt(2)/sqrt(3)])):
  OneEigenSpaceDim(Getas1,I);
end if;
   
