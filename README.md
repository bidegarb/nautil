# Aim

NAUtil aims at automating the __stability analysis of linear finite difference schemes.__
It has been developed to treat electromagnetic problems but can be practically extended to any application provided it is discretized by a linear finite difference scheme. Collaboration in any application field is seeked.

The different routines and samples have to be used in a Maple environment.
Maple 10 has been used for the development. 
Routines have not been tested on earlier Maple versions, but it is not totally impossible that it works also. 

# Principle

Since we only deal with linear models, we can analyze them in the frequency domain. 
Thus we assume that the scheme handles a single (vector valued) variable <i>U<sub>j</sub><sup>n</sup></i> with spatial dependence <i>U<sub>j</sub><sup>n</sup> = U<sup>n</sup> exp(i&xi;j)</i>. 
The scheme is then described as <i>U<sup>n+1</sup>=GU<sup>n</sup></i> and in our case the amplification matrix <i>G</i> does not depend on time or on the space steps <i>&delta;x</i> and time step <i>&delta;t</i> separately but only on the ratio <i>&delta;x/&delta;t</i>. 
This ensures that <i>U<sup>n</sup>=G<sup>n</sup>U<sup>0</sup></i> and stability comes to the boundedness of <i>G<sup>n</sup></i>.

A necessary stability condition is that the eigenvalues of <i>G</i> lie in the unit disk. 
Only eigenvalues on the unit circle can cause instabilities and two cases can occur: 
1. the eigenvalues of modulus 1 are simple and the scheme is stable. 
Stability analysis is performed on the characteristic polynomial of <i>G</i> using von Neumann analysis; 
2. some eigenvalues of modulus 1 are multiple and stability is obtained if and only if the associated minimal subspaces are of dimension 1. 
The analysis of the characteristic polynomial does not yield any information on the associated subspaces. 
Matrix <i>G</i> has to be studied. 
In our study, this corresponds to degenerate cases for which matrix <i>G</i> has always a very specific form and minimal subspaces are easy to determine. 

# Method: von Neumann analysis

The localization of roots of polynomials is a difficult task if the polynomial has a large degree and/or coefficients depending on many parameters. 
This the case in our context. 
We split this difficult problem into many simpler ones.

Given a polynomial <i>&phi;<sub>0</sub></i>, we construct a sequence   

<center>
&phi;<sub>m+1</sub>(z) = ( &phi;<sub>m</sub><sup>*</sup>(0)&phi;<sub>m</sub>(z) - &phi;<sub>m</sub>(0)&phi;<sub>m</sub><sup>*</sup>(z) ) / z.
</center>

This sequence has a decreasing order, <u>but</u> the dependence of the coefficients in the parameters is becoming more intricate.

__Definition:__
A <u>Schur polynomial</u> has all its roots <i>r</i> inside the unit circle: <i>|r|<1</i>.

__Theorem:__
A polynomial <i>&phi;<sub>m</sub></i> is a Schur polynomial of exact degree <i>d</i> if and only if <i>&phi;<sub>m+1</sub></i> is a Schur polynomial of exact degree <i>d-1</i> and <i>|&phi;<sub>m</sub>(0)|&le;|&phi;<sub>m</sub><sup>*</sup>(0)|</i>.

__Definition:__
A <u>simple von Neuman polynomial</u> has all its roots <i>r</i> in the unit disk: <i>|r|&le;1</i> and eigenvalues of unit modulus are simple.

__Theorem:__
A polynomial <i>&phi;<sub>m</sub></i> is a simple von Neumann polynomial if and only if <br>
(i) <i>&phi;<sub>m+1</sub></i> is a simple von Neumann polynomial and <i>|&phi;<sub>m</sub>(0)|&le;|&phi;<sub>m</sub><sup>*</sup>(0)|</i>;<br>
or (ii) <i>&phi;<sub>m+1</sub></i> is identically zero and <i>&phi;<sub>m</sub>'</i> is a Schur polynomial.

At each step, we have to check condition <i>|&phi;<sub>m</sub>(0)|&le;|&phi;<sub>m</sub><sup>*</sup>(0)|</i> and degree. 
This is comparatively simpler than locating the roots. 
But these are not simple problems either. 
We now have to determine the sign of a polynomial with coefficents depending on the parameters. 
These parameters often lie in an interval, e.g. the ratio <i>&delta;t/&delta;x</i> or <i>&delta;t/&delta;x<sup>2</sup></i> (the stability condition often reads like this); or are positive, e.g. physical constants.

# News

- July 13th 2006: First &beta;-version online
- September 2006: Version 1.0.0 online
