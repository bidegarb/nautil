################################################################################
## NAUtil Toolbox                                                             ##
## Author: Brigitte BIDEGARAY-FESQUET                                         ##
## Date: September 2006                                                       ##
##                                                                            ##
## File Adim: Towards dimensionless and frequential equations                 ##
################################################################################

# Function CalcVar
#
# Selects routine according to physical model

CalcVar := proc(Dim,Polar,Formula,Model,Var,VarBack)
  local VarDecalX,VarUold_common,VarUnew_common,VarXi,VarReturn:

  CommonVar(Dim,Polar,VarDecalX,VarUold_common,VarUnew_common):

  if Model="MaxDebye" then
    Var := CalcVarDebye(Dim,Polar,Formula,VarDecalX,VarUold_common,VarUnew_common)
  end if:

  if Model="MaxLorentz" then
    Var := CalcVarLorentz(Dim,Polar,Formula,VarDecalX,VarUold_common,VarUnew_common)
  end if:

  if Dim = 1 then
    VarXi := [exp(I*xi) = sigma/lambda+1, 
              exp(-I*xi) = q/(sigma*lambda)+1, 
              cos(xi) = 1-1/2*q/lambda^2]:
    Var := [op(op(Var)),VarXi];
    VarBack := [q = sigma*conjugate(sigma)];
  end if:

  if Dim = 2 then
    VarXi := [exp(I*xix) = sigmax/lambdax+1, 
              exp(-I*xix) = qx/(sigmax*lambdax)+1, 
              cos(xix) = 1-1/2*qx/lambdax^2,
              exp(I*xiy) = sigmay/lambday+1, 
              exp(-I*xiy) = (q-qx)/(sigmay*lambday)+1, 
              cos(xiy) = 1-1/2*(q-qx)/lambday^2]:
    Var := [op(op(Var)),VarXi];
    VarBack := [qx = sigmax*conjugate(sigmax), 
                q = sigmax*conjugate(sigmax) 
                  + sigmay*conjugate(sigmay)];
  end if:

  if Dim = 3 then
    VarXi := [exp(I*xix) = sigmax/lambdax+1, 
              exp(-I*xix) = qx/(sigmax*lambdax)+1, 
              cos(xix) = 1-1/2*qx/lambdax^2,
              exp(I*xiy) = sigmay/lambday+1, 
              exp(-I*xiy) = qy/(sigmay*lambday)+1, 
              cos(xiy) = 1-1/2*qy/lambday^2,
              exp(I*xiz) = sigmaz/lambdaz+1, 
              exp(-I*xiz) = (q-qx-qy)/(sigmaz*lambdaz)+1, 
              cos(xiz) = 1-1/2*(q-qx-qy)/lambdaz^2]:
    Var := [op(op(Var)),VarXi,VarReturn];
    VarBack := [qx = sigmax*conjugate(sigmax), 
                qy = sigmay*conjugate(sigmay),
                q = sigmax*conjugate(sigmax) 
                  + sigmay*conjugate(sigmay)
                  + sigmaz*conjugate(sigmaz)];
  end if:
end proc:

################################################################################

# Function CommonVar
#
# Defines new dimensionless variables for the system common for all models

CommonVar := proc(Dim,Polar,VarDecalX,VarUold_common,VarUnew_common)

  if Dim=1 then
    VarDecalX := [Ez[n,j-1/2]   = Ez[n,j+1/2]*exp(-I*xi), 
                  By[n+1/2,j+1] = By[n+1/2,j]*exp(I*xi),
                  By[n-1/2,j+1] = By[n-1/2,j]*exp(I*xi)]:
    VarUold_common := [By[n-1/2,j]     = Uold[1]/cinfini, 
                       Ez[n,j+1/2]     = Uold[2]]: 
    VarUnew_common := [By[n+1/2,j]     = Unew[1]/cinfini, 
                       Ez[n+1,j+1/2]   = Unew[2]]:
  end if:

  if (Dim=2 and Polar="TE") then
    VarDecalX := [Ez[n,j-1/2,k+1/2]   = Ez[n,j+1/2,k+1/2]*exp(-I*xix), 
                  Ez[n,j+1/2,k-1/2]   = Ez[n,j+1/2,k+1/2]*exp(-I*xiy), 
                  Bx[n+1/2,j+1/2,k+1] = Bx[n+1/2,j+1/2,k]*exp(I*xiy),
                  By[n+1/2,j+1,k+1/2] = By[n+1/2,j,k+1/2]*exp(I*xix),
                  Bx[n-1/2,j+1/2,k+1] = Bx[n-1/2,j+1/2,k]*exp(I*xiy),
                  By[n-1/2,j+1,k+1/2] = By[n-1/2,j,k+1/2]*exp(I*xix)]:
    VarUold_common := [Bx[n-1/2,j+1/2,k]   = Uold[1]/cinfini, 
                       By[n-1/2,j,k+1/2]   = Uold[2]/cinfini, 
                       Ez[n,j+1/2,k+1/2]   = Uold[3]]: 
    VarUnew_common := [Bx[n+1/2,j+1/2,k]   = Unew[1]/cinfini, 
                       By[n+1/2,j,k+1/2]   = Unew[2]/cinfini, 
                       Ez[n+1,j+1/2,k+1/2] = Unew[3]]:
  end if:

  if (Dim=2 and Polar="TM") then
    VarDecalX := [Ex[n,j,k-1/2] = Ex[n,j,k+1/2]*exp(-I*xiy), 
                  Ey[n,j-1/2,k] = Ey[n,j+1/2,k]*exp(-I*xix), 
                  Bz[n+1/2,j,k+1] = Bz[n+1/2,j,k]*exp(I*xiy),
                  Bz[n+1/2,j+1,k] = Bz[n+1/2,j,k]*exp(I*xix),
                  Bz[n-1/2,j,k+1] = Bz[n-1/2,j,k]*exp(I*xiy),
                  Bz[n-1/2,j+1,k] = Bz[n-1/2,j,k]*exp(I*xix)]:
    VarUold_common := [Bz[n-1/2,j,k] = Uold[1]/cinfini, 
                       Ex[n,j,k+1/2]         = Uold[2], 
                       Ey[n,j+1/2,k]         = Uold[3]]: 
    VarUnew_common := [Bz[n+1/2,j,k] = Unew[1]/cinfini, 
                       Ex[n+1,j,k+1/2]       = Unew[2], 
                       Ey[n+1,j+1/2,k]       = Unew[3]]:

  end if:

  if Dim=3 then
    VarDecalX := [Bx[n-1/2,j+1/2,k+1,l] = Bx[n-1/2,j+1/2,k,l]*exp(I*xiy), 
                  Bx[n-1/2,j+1/2,k,l+1] = Bx[n-1/2,j+1/2,k,l]*exp(I*xiz), 
                  By[n-1/2,j+1,k+1/2,l] = By[n-1/2,j,k+1/2,l]*exp(I*xix),
                  By[n-1/2,j,k+1/2,l+1] = By[n-1/2,j,k+1/2,l]*exp(I*xiz), 
                  Bz[n-1/2,j+1,k,l+1/2] = Bz[n-1/2,j,k,l+1/2]*exp(I*xix), 
                  Bz[n-1/2,j,k+1,l+1/2] = Bz[n-1/2,j,k,l+1/2]*exp(I*xiy), 
                  Bx[n+1/2,j+1/2,k+1,l] = Bx[n+1/2,j+1/2,k,l]*exp(I*xiy), 
                  Bx[n+1/2,j+1/2,k,l+1] = Bx[n+1/2,j+1/2,k,l]*exp(I*xiz), 
                  By[n+1/2,j+1,k+1/2,l] = By[n+1/2,j,k+1/2,l]*exp(I*xix),
                  By[n+1/2,j,k+1/2,l+1] = By[n+1/2,j,k+1/2,l]*exp(I*xiz), 
                  Bz[n+1/2,j+1,k,l+1/2] = Bz[n+1/2,j,k,l+1/2]*exp(I*xix), 
                  Bz[n+1/2,j,k+1,l+1/2] = Bz[n+1/2,j,k,l+1/2]*exp(I*xiy), 
                  Ex[n,j,k-1/2,l+1/2]   = Ex[n,j,k+1/2,l+1/2]*exp(-I*xiy),
                  Ex[n,j,k+1/2,l-1/2]   = Ex[n,j,k+1/2,l+1/2]*exp(-I*xiz),
                  Ey[n,j-1/2,k,l+1/2]   = Ey[n,j+1/2,k,l+1/2]*exp(-I*xix),
                  Ey[n,j+1/2,k,l-1/2]   = Ey[n,j+1/2,k,l+1/2]*exp(-I*xiz),
                  Ez[n,j-1/2,k+1/2,l]   = Ez[n,j+1/2,k+1/2,l]*exp(-I*xix),
                  Ez[n,j+1/2,k-1/2,l]   = Ez[n,j+1/2,k+1/2,l]*exp(-I*xiy)]:
    VarUold_common := [Bx[n-1/2,j+1/2,k,l]   = Uold[1]/cinfini, 
                       By[n-1/2,j,k+1/2,l]   = Uold[2]/cinfini, 
                       Bz[n-1/2,j,k,l+1/2]   = Uold[3]/cinfini, 
                       Ex[n,j,k+1/2,l+1/2]   = Uold[4],
                       Ey[n,j+1/2,k,l+1/2]   = Uold[5],
                       Ez[n,j+1/2,k+1/2,l]   = Uold[6]]: 
    VarUnew_common := [Bx[n+1/2,j+1/2,k,l]   = Unew[1]/cinfini, 
                       By[n+1/2,j,k+1/2,l]   = Unew[2]/cinfini, 
                       Bz[n+1/2,j,k,l+1/2]   = Unew[3]/cinfini, 
                       Ex[n+1,j,k+1/2,l+1/2] = Unew[4],
                       Ey[n+1,j+1/2,k,l+1/2] = Unew[5],
                       Ez[n+1,j+1/2,k+1/2,l] = Unew[6]]:
  end if:
end proc:

################################################################################

